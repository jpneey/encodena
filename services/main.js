$(function(){

    navbar();
    loading();
});

function navbar() {
    var space = $(".navbar").outerHeight();
    $(".navbar-spacer").css({
        'height' : space
    })
}

function loading() {

    $('#a-e-btn').on('click', function(){

        $('*').css({
            'pointer-events' : 'none',
            'cursor' : 'wait'
        })
        $("#e-btn").text("Exporting    ");
        $("#e-btn-i").removeClass();
        $("#e-btn-i").addClass("fas");
        $("#e-btn-i").addClass("fa-spinner");
        $("#e-btn-i").addClass("fa-spin");


    })
}