$(function () {

    var form =  $("#ajax-form");
    form.submit(function(event){
        $('#submit').val("sending . . .")
        $('#submit').attr("disabled", true);

        
        event.preventDefault();
            
        $.ajax({
            type: form.attr("method"),
            url: form.attr("action"),
            data: form.serialize(),

            success: function (data) {
                alert(data);
                $("#ajax-form").load(location.href + " #ajax-form>*", ""); 
                $('#submit').val("Sent");
                $('#submit').attr("disabled", true);
            },
            
            error: function(data){
                $('#submit').val("resend");
            }
        });
    })
})