<?php
session_start();
error_reporting(0);
if (isset($_SESSION['admin'])) {
    require_once("controller/dbcontroller.php");
    $db_handle = new DBController();

    if(!empty($_GET["action"])) {
        switch ($_GET["action"]) {

            case "execute":
                
                $currentDir = getcwd();
                $uploadDirectory = "/images/";
                $rand   = substr(md5(uniqid(mt_rand(), true)), 0, 3); 
            
                $errors = []; // Store all foreseen and unforseen errors here
            
                $fileExtensions = ['jpeg','jpg','png']; // Get all the file extensions
            
                $fileName = $_FILES['myfile']['name'];
                $fileSize = $_FILES['myfile']['size'];
                $fileTmpName  = $_FILES['myfile']['tmp_name'];
                $fileTmpName2  = $_FILES['img_description']['tmp_name'];
                $fileType = $_FILES['myfile']['type'];
                $fileExtension = strtolower(end(explode('.',$fileName)));

                $temp = explode(".", $_FILES["myfile"]["name"]);
                $newfilename = round(microtime(true)) . '.' . end($temp);

                $temp2 = explode(".", $_FILES["img_description"]["name"]);
                $newfilename2 = round(microtime(true)) . 'des.' . end($temp2);

            
                $nyes = "images/" . basename($fileName);
                $randomname = "images/" . basename($newfilename);
                $randomname2 = "images/" . basename($newfilename2);
            
                
                $uploadPath = $currentDir . $uploadDirectory . basename($newfilename) ; 
                $uploadPath2 = $currentDir . $uploadDirectory . basename($newfilename2) ; 

                //gallery
                $mcrt = round(microtime(true));
                for($i=0;$i <= 5; $i++) {    
                    ${"gallery".$i."TmpName"}  = $_FILES['gallery'.$i]['tmp_name'];
                    ${"gallery".$i} = explode(".", $_FILES['gallery'.$i]["name"]);
                    ${"gallery".$i."rand"} = $mcrt . $rand . 'gal'. $i . '.' . end(${"gallery".$i});
                    ${"gallery".$i."path"} = $currentDir . $uploadDirectory . basename(${"gallery".$i."rand"});
                }


                    if (! in_array($fileExtension,$fileExtensions)) {
                        $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
                    }
            
                    if ($fileSize > 2000000) {
                        $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
                    }
            
                    if (empty($errors)) {
                        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);
                        $didUpload2 = move_uploaded_file($fileTmpName2, $uploadPath2);
                        
                        for($i=0;$i <= 5; $i++) {  
                            
                            ${"gallery".$i} = move_uploaded_file(${"gallery".$i."TmpName"}, ${"gallery".$i."path"});
                        
                        }
            
                        if ($didUpload && $didUpload2) {
                            
                            $nme = $_POST["name"];
                            $stripped = substr($nme, 0, 1);
                            $rand   = $stripped . substr(md5(uniqid(mt_rand(), true)), 0, 3);     
                            $uniqrand   = uniqid();
                            $code = $rand . $uniqrand . $stripped;
                            
                            $filepath = $randomname; //image_main
                            $filepath2 = $randomname2; //img_des
                            
                            for($i=1;$i <= 5; $i++) { 
                                if(${"gallery".$i}) {
                                    ${"gallery".$i} = 'images/' . ${"gallery".$i."rand"};
                                } else {
                                    ${"gallery".$i} = 'images/gallery.src';
                                }
                            } 

                            $postName = $_POST["name"];
                            $postDescription = $_POST["description"];
                            $postCategory = $_POST["category"];
                            $postBrand = $_POST["brand"];
                            $postPcode = $_POST["pcode"];
                            $variation = $_POST["variation"];
                            
                            $add = $db_handle->runQuery("INSERT INTO products(name, description, price, category, image, code, brand, img_description, product_code, image_gallery1, image_gallery2, image_gallery3, image_gallery4, image_gallery5, variations) VALUES('$postName', '$postDescription', '0', '$postCategory', '$filepath', '$code', '$postBrand', '$filepath2', '$postPcode', '$gallery1', '$gallery2', '$gallery3', '$gallery4', '$gallery5', '$variation')");
                            
                            header("location: add.php?prime=" . $filepath . "&success=true");
                            exit;
                        }
                    }
                header("location: add.php?prime=" . $filepath . "&success=imageerror");
                exit;
                break;
            case "upload":
                header("location: add.php?prime=" . $nyes . "&success=imageerror");
                break;
        }
    }
?>


<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="images/icon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css">
        <link rel="stylesheet" type="text/css" href="styles/cart.css">
        
        
        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>
    </head>
    <body>
        <div class="navigation" style="background: #2d2d2d;">
            <div class="navigation-menu">
                <ul id="horizontal-list">
                    <li><a href="admin.php" style="color: #ffffff; border-left: 5px solid #2d2d2d;">DashBoard</a></li>
                    <li><a href="admin.php?a34xcvdm23in56yu89on=logout" style="color: #ffffff; border-left: 5px solid #2d2d2d;"><i class="fas fa-user"></i><span class="tq filled">Log Out</span></a></li>
                </ul>
            </div>
        </div>
        <div class="home-banner">
        
        </div>
        <?php 
            if($_GET["success"] == 'imageerror') { ?>
            
            <div class="no-records">Image Upload failed. Please make sure that the file format is .jpg .jpeg .png or the file size is lower than 2mb</div>
            <?php
            }
            if ($_GET["success"] == 'metaimageerror') { ?>
            <div class="no-records warning">Meta description updated except image description.</div>
            <?php
            }
            if ($_GET["success"] == 'true') {
            ?>
            <div class="yes-records warning">Product Succesfully added.</div>
            <?php
            }
        ?>
        <div class="product-view-container">
            <form action="add.php?action=execute" method="POST" enctype="multipart/form-data" id="nyes">
                <div class="product-view">
                    <img src="" class="view-img-main"/>
                    <div class="centered">
                            <input required type="file" name="myfile" id="fileToUpload" accept="image/*">
                            <label for="fileToUpload" class="button">
                                <i class="fas fa-cloud-upload-alt"></i> Add Image
                            </label>
                    </div>

                    <div>
                        <div class="yes-records">Image Gallery :</div>
                          
                        
                        <input type="file" name="gallery1" accept="image/*">
                        <input type="file" name="gallery2" accept="image/*">
                        <input type="file" name="gallery3" accept="image/*">
                        <input type="file" name="gallery4" accept="image/*">
                        <input type="file" name="gallery5" accept="image/*">
                    </div>
                </div>

                <div class="product-view">
                    <div class="view-text-container">
                        <p class="align-left"><input required type="text" name="name" placeholder="Name"></p>
                        <p class="align-left"><input required name="description" type="text" placeholder="Short description"></p>
                        <p>Product Variaton:<br><i>select only if needed. Choose 'none' for default</i></p>
                        
                        <p class="align-left">
                            <select required name="variation">
                                <option value="none" selected>none</option>
                                <?php 
                                    $variations = $db_handle->runQuery("SELECT * FROM variations ORDER BY id ASC");
                                    if (!empty($variations)) { 
                                        foreach($variations as $key=>$value){
                                    ?>
                                        <option value="<?php echo $variations[$key]["id"] ?>"><?php echo $variations[$key]["name"] ?></option>
                                    <?php 
                                        }
                                    }
                                ?>
                            </select>
                        </p>

                        <p class="align-left">
                            <select required name="category" type="text" placeholder="category">
                                
                                <option value="uncategorized" selected>Category</option>
                                <?php 
                                $category = $db_handle->runQuery("SELECT * FROM category ORDER BY id ASC");
                                if (!empty($category)) { 
                                    foreach($category as $key=>$value){
                                ?>
                                    <option value="<?php echo $category[$key]["category"] ?>"><?php echo $category[$key]["category"] ?></option>
                                <?php 
                                    }
                                }
                                ?>
                            </select>
                        </p>
                        <p class="align-left"><input required name="pcode" type="text" placeholder="Product code"></p>
                        <p class="align-left"><input required name="brand" type="text" placeholder="Brand"></p>
                        
                        <p>product image description:</p>
                        <p>(500px X 850px)</p>
                        <input required type="file" name="img_description" id="filedescription"  accept="image/*">
                        <img src="" class="view-img-main desfile"/>
                    </div>
                </div>
                
                <input type="submit" value="Add Product" class="button align-center save-product"/>
                
                <script>
                    
                    $("#fileToUpload").change(function () {
                        filePreview(this);
                    }); 
                    $("#filedescription").change(function () {
                        desfilePreview(this);
                    }); 
                    function filePreview(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $('.product-view > img').attr("src", "" + e.target.result + "");
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                    function desfilePreview(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $('.desfile').attr("src", "" + e.target.result + "");
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                      
                </script>
            </form> 
        </div>
    </body>
</html>
<?php 
    }
    else {

        header('location: ad-login.php?toast=attempt');
        exit;
    }
?>