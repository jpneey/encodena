﻿<?php
session_start();
require_once("controller/dbcontroller.php");
$db_handle = new DBController();

if(!empty($_GET["action"])) {

    switch($_GET["action"]) {
        case "add":
            if(!empty($_POST["quantity"])) {
                $productById = $db_handle->runQuery("SELECT * FROM products WHERE id='" . $_GET["id"] . "' ");
                $itemArray = array($productById[0]["code"]=>array('name'=>$productById[0]["name"], 'id'=>$productById[0]["code"], 'quantity'=>$_POST["quantity"], 'image'=>$productById[0]["image"]));

                if(!empty($_SESSION["cart_item"])) {
                    if(in_array($productById[0]["code"], array_keys($_SESSION["cart_item"]))) {

                        foreach($_SESSION["cart_item"] as $k => $v) {

                            if($productById[0]["code"] == $k) {
                                if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                            }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }

                
            header('Location: index.php');
            exit;
            
            }
            break;

        case "remove":
            if(!empty($_SESSION["cart_item"])) {
                foreach($_SESSION["cart_item"] as $k => $v) {
                        if($_GET["id"] == $k)
                            unset($_SESSION["cart_item"][$k]);				
                        if(empty($_SESSION["cart_item"]))
                            unset($_SESSION["cart_item"]);
                }
            }
            header('Location: index.php');
            exit;
	        break;

        case "empty":
            unset($_SESSION["cart_item"]);
            header('Location: index.php');
            exit;
            
	        break;	
    }
}
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Inmed Corporation</title>
        <meta name="description" content="">
        <meta name="author" content="John Paul Burato">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" type="image/png" href="images/icon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,800&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="styles/common.css">
        <link rel="stylesheet" type="text/css" href="styles/main.css?v1.1">

        <link rel="stylesheet" type="text/css" href="banner/component.css">
        <script src="scripts/jquery.js"></script>
        <script src="scripts/main.js"></script>

        <script src="banner/a.js"></script>
        <script src="banner/b.js"></script>
        

    </head>
    <body>
        
        <?php 
        ?>
        <div class="navigation">
            <img src="images/logo.png"/>
            
            <div class="mob-menu">
                <i class="fas fa-bars" onclick="sideNav(3)"></i>
            </div>
            <div class="navigation-menu">
                <ul id="horizontal-list">
                    <li><a href="../../index.html">Home</a></li>
                    <li><a href="index.php">Products</a></li>
                    <li><a href="#!" onclick="sideNav(1)">Categories</a></li>
                    <?php 
                        if(isset($_SESSION["cart_item"])) {
                            $total_quantity = 0;
                            foreach ($_SESSION["cart_item"] as $item) {
                                $total_quantity += $item["quantity"];
                            }
                    ?>
                    <li id="cart-menu"><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq filled"><?php echo $total_quantity; ?> Item(s) - for quote</span></a></li>
                    <?php 
                        } else { ?>
                        <li><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq empty">I'm empty :(</span></a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="sidemob">
            <div class="sidenavmob">
                <a onclick="sideNav(4)"><i class="fas fa-times"></i></a>
                <a href="../../index.html">Home</a>
                <a href="index.php">Products</a>
                <a href="#!" onclick="sideNav(1)">Categories</a>
                <?php 
                    if(isset($_SESSION["cart_item"])) {
                        $total_quantity = 0;
                        foreach ($_SESSION["cart_item"] as $item) {
                            $total_quantity += $item["quantity"];
                        }
                ?>
                <a id="cart-menu"><a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="cart-count-menu"> <?php echo $total_quantity; ?> Item(s)</span></a></a>
                <?php 
                    } else { ?>
                    <a href="cart.php"><i class="fas fa-shopping-cart"></i><span class="tq empty">I'm empty :(</span></a>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="sidenav">
            <div>
                <a onclick="sideNav(2)"><i class="fas fa-times"></i></a>
                <?php
                $product_category = $db_handle->runQuery("SELECT * FROM category ORDER BY id ASC");
                if (!empty($product_category)) { 
                    foreach($product_category as $key=>$value){
                ?>
                    <a href="viewcategory.php?category=<?php echo $product_category[$key]['category']?>"><?php echo $product_category[$key]['category']?></a>
                <?php 
                    }
                } else {

                }
                
                ?>
            </div>
        </div>
        <div class="home-banner"></div>
        <div class="category-banner bged" id="mobile-banner">
            <div class="centered">
                <h1 class="titles i-cwhite" style="text-align: center">Our Latest Products</h1>
            </div>
        </div>
        <div class="page-wrapper">
            <div id="cbp-fwslider" class="cbp-fwslider pc-banner">
                <ul>
                    <li><a href="http://inmed.com.ph/pages/products/view.php?view=3"><img src="images/banner/pro2.jpg" alt="img05"/></a></li>
                    <li><a href="http://inmed.com.ph/pages/products/view.php?view=27"><img src="images/banner/pro6.jpg" alt="img01"/></a></li>
                    <li><a href="http://inmed.com.ph/pages/products/view.php?view=28"><img src="images/banner/pro5.jpg" alt="img02"/></a></li>
                    <li><a href="http://inmed.com.ph/pages/products/view.php?view=29"><img src="images/banner/pro4.jpg" alt="img03"/></a></li>
                    <li><a href="http://inmed.com.ph/pages/products/view.php?view=30"><img src="images/banner/pro3.jpg" alt="img04"/></a></li>
                </ul>
            </div>
        <script>
            $(document).ready(function(){
                $('#cbp-fwslider').cbpFWSlider();
                setInterval( function(){
                    if($('.cbp-fwdots').find('span:last').hasClass('cbp-fwcurrent')){
                        $('.cbp-fwdots').find('span:first-child').click();
                    } else { $('.cbp-fwnext').click();}
                }, 9000 );
            });
        </script>
        <div class="product-container">
            <div class="product-grid-container">
            <?php
            $product_array = $db_handle->runQuery("SELECT * FROM products ORDER BY id DESC LIMIT 8");
            if (!empty($product_array)) { 
                foreach($product_array as $key=>$value){
            ?>
                <div class="product-grid">
                    <a href="view.php?view=<?php echo $product_array[$key]["id"]; ?>" class="i-cwhite">
                    <div class="image">
                        <img class="image-true" src="<?php echo $product_array[$key]["image"]; ?>" />
                    </div></a>

                    <h1><?php echo $product_array[$key]["name"]; ?></h1>
                    <p class="description"><?php echo $product_array[$key]["description"]; ?></p>
                    <form method="post" id="cart-ajax" action="index.php?action=add&id=<?php echo $product_array[$key]["id"];?>">
                        <div class="cart-action">
                            <input type="number" class="product-quantity" name="quantity" value="1" onkeypress="return isNumberKey(event)" min="1" hidden/>
                            <input type="submit" value="Quote" class="quotebutton" style="width: 90%;"/>
                        </div>
                    </form>
                </div>
            <?php
                }
            }
            ?>
            </div>
        </div>
        </div>
        <?php 
            require("footer.php");
        ?>
    </body>
</html>