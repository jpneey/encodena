    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Encode Na!</title>
	<meta name="author" content="John Paul Burato, John Dominique Engson" />
	<meta name="description" content="" />
	<meta name="keywords"  content=""/>
	<meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="" type="image/x-icon">
    
    <link rel="stylesheet" type="text/css" href="./vendor/css/all.css">
    
    <link rel="stylesheet" type="text/css" href="./styles/main.css" />
	<script src="./vendor/jquery.min.js"></script>
    
    <script src="./services/ajax.js"></script>
    <script src="./services/main.js"></script>