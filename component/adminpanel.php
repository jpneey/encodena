<?php ?>
<div class="row">
    
                <div class="column">
                    <div class="card">
                        <div class="">
                            <p class="mini-title">Export<br>Today's list</p>
                            <a href="controller/export_today.php?today=true" id="a-e-btn"><button id="e-btn" class="button b-green">export &emsp14; <i id="e-btn-i" class="fas fa-file-export"></i></button></a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="">
                            <p class="mini-title">Export<br>Everything</p>
                            <a href="#!"><button class="button b-green">export &emsp14; <i class="fas fa-file-export"></i></button></a>
                        </div>
                    </div>
                </div>
                <div class="column">
                    
                     <div class="card">
                        <p class="mini-title">Search By Name</p>
                        <form action="result.php?action=search" method="POST" class="search-form">
                            <input type="search" name="search" placeholder="search" class="search-l"/>
                            <button class="search-r"><i class="fas fa-search"></i></button>
                        </form>
                    </div>

                    
                    <div class="card">
                        <p class="mini-title">Filter by month</p>
                        <form action="result.php?action=filter" method="POST" class="search-form">
                            <select required name="month">
                                <option value="1" selected>January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                            <select required name="year">
                                <option value="2019" selected>2019</option>
                                <option value="2019">2020</option>
                                <option value="2019">2021</option>
                                <option value="2019">2022</option>
                                <option value="2019">2023</option>
                                <option value="2019">2024</option>
                                <option value="2019">2025</option>
                            </select>
                            <input id="submit" type="submit" value="Search"/>
                        </form>
                    </div>

                </div>
                <div class="column">
                    
                    <div class="card">
                        <div class="">
                            <p class="mini-title">View all<br>application entry</p>
                            <a href="result.php?action=list"><button class="button b-green">view &emsp14; <i class="fas fa-chevron-right"></i></button></a>
                        </div>
                    </div>

                    <div class="card">
                        <div class="">
                            <p class="mini-title">DB Action</p>
                            <a href="#!"><button class="button b-orange">backup &emsp14; <i class="fas fa-file-export"></i></button></a>
                            <a href="#!"><button class="button b-red">reset &emsp14; <i class="fas fa-power-off"></i></button></a>
                        </div>
                    </div>


                </div>

            </div>
<?php ?>