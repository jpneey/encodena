<?php 
    session_start();
    if (isset($_SESSION["auth"])) {
            
        require_once("../controller/dbcontroller.php");
        $id = $_GET["id"];
        $db_handle = new DBController();
        $application = $db_handle->runQuery("SELECT * FROM registry WHERE person_id = '" . $id . "'");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Encode Na!</title>
	<meta name="author" content="John Paul Burato, John Dominique Engson" />
	<meta name="description" content="" />
	<meta name="keywords"  content=""/>
	<meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="" type="image/x-icon">
    
    <link rel="stylesheet" type="text/css" href="../vendor/css/all.css">
    <link rel="stylesheet" type="text/css" href="../styles/main.css" />
    <link rel="stylesheet" type="text/css" href="../styles/frontend.css" />

	<script src="../vendor/jquery.min.js"></script>
    
    <script src="../services/ajax.js"></script>
    <script src="../services/main.js"></script>

    <link rel="stylesheet" type="text/css" href="../vendor/jquery-ui.css" />
    <script src="../vendor/jquery-ui.min.js"></script>
    <script src="../services/datepicker.js"></script>
    
</head>
<body>
    
    <div class="content-wrapper">
        <div class="container">
            <?php
            if (!empty($application)) { 
                foreach($application as $key=>$value){
            ?>
            <form id="ajax-form" class="form" action='../controller/post_form_update.php?personid=<?php echo $application[$key]["person_id"]; ?>' method="POST" enctype="multipart/form-data">
                <p class="title">Update </p>

                <div>First Name:</div>
                <input required type="text" name="fname" value="<?php echo $application[$key]["fname"]; ?>"/>
                <div>Middle Name:</div>
                <input required type="text" name="mname" value="<?php echo $application[$key]["lname"]; ?>"/>
                <div>Last Name:</div>
                <input required type="text" name="lname" value="<?php echo $application[$key]["mname"]; ?>"/>
                <div>Birthday:</div>
                <input class="uppercase" required type="date" name="birthday" value="<?php echo $application[$key]["birthday"]; ?>"/>
                <div>Address:</div>
                <input required type="text" name="address" value="<?php echo $application[$key]["address"]; ?>"/>
                <div>Phone number:</div>
                <input required type="number" name="phone_number" value="<?php echo $application[$key]["mobile"]; ?>"/>
                <div>E-mail:</div>
                <input class="text-none" required type="email" name="email" value="<?php echo $application[$key]["email"]; ?>"/>
                
                <div>Gender:</div>

                <select required name="gender">
                    <option value="<?php echo $application[$key]["gender"]; ?>" selected><?php echo $application[$key]["gender"]; ?> </option>
                    <option value="male">male</option>
                    <option value="female">female</option>
                </select>
                
                <div>Civil Status:</div>
                <select required name="civil_status">
                    <option value="<?php echo $application[$key]["civil_status"]; ?>" selected><?php echo $application[$key]["civil_status"]; ?></option>
                    <option value="single">single</option>
                    <option value="married">married</option>
                    <option value="divorced">divorced</option>
                    <option value="others">others</option>
                </select>

                <hr>
                
                <div>SSS:</div>
                <input required type="text" name="sss" value="<?php echo $application[$key]["sss"]; ?>"/>
                <div>Philhealth:</div>
                <input required type="text" name="philhealth" value="<?php echo $application[$key]["philhealth"]; ?>"/>
                <div>Pagibig:</div>
                <input required type="text" name="pagibig" value="<?php echo $application[$key]["pagibig"]; ?>"/>
                <div>TIN:</div>
                <input required type="text" name="tin" value="<?php echo $application[$key]["tin"]; ?>"/>
                <div>NBI:</div>
                <input required type="text" name="nbi" value="<?php echo $application[$key]["nbi"]; ?>"/>
                <div>Police Clearance:</div>
                <input required type="text" name="police_clearance" value="<?php echo $application[$key]["police_clearance"]; ?>"/>

                <hr>
                

                <div>Position:</div>
                <input required type="text" name="position" value="<?php echo $application[$key]["position"]; ?>"/>
                
                <input id="submit" type="submit" value="post"/>






                <a href="../admin.php" class="a">back to dashboard <i class="fas fa-long-arrow-alt-right"></i></a>
            </form>
            <?php 
                    }
                } else { ?>
            <p class="auth-error popped"><br>id not find.<br>This means that the entry you want to edit<br>is moved / deleted or didn't actually exists .<br> &nbsp;</p>
            <a href="../admin.php" class="a">back to dashboard <i class="fas fa-long-arrow-alt-right"></i></a>
            <?php
            }
            ?>
        </div>
    </div>


</body>
</html>
<?php
    }
    else {
        header('location: ./login.php');
    }
?>
