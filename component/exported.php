<?php 
    session_start();
    if (isset($_SESSION["auth"])) {
            
        require_once("../controller/dbcontroller.php");
        $id = $_GET["id"];
        $db_handle = new DBController();
        $application = $db_handle->runQuery("SELECT * FROM registry WHERE person_id = '" . $id . "'");

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Encode Na!</title>
	<meta name="author" content="John Paul Burato, John Dominique Engson" />
	<meta name="description" content="" />
	<meta name="keywords"  content=""/>
	<meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="" type="image/x-icon">
    
    <link rel="stylesheet" type="text/css" href="../vendor/css/all.css">
    <link rel="stylesheet" type="text/css" href="../styles/main.css" />
    <link rel="stylesheet" type="text/css" href="../styles/frontend.css" />

	<script src="../vendor/jquery.min.js"></script>
    
    <script src="../services/ajax.js"></script>
    <script src="../services/main.js"></script>

    <link rel="stylesheet" type="text/css" href="../vendor/jquery-ui.css" />
    <script src="../vendor/jquery-ui.min.js"></script>
    <script src="../services/datepicker.js"></script>
    
</head>
<body>
    <div class="centered card-reusable">
    <?php 
        if(isset($_GET["today"])) { ?>
            <p class="a mini-title popped">Export Success! View the excel file at folder: htdocs / controller / Excel / exported_db_<?php echo $_GET["today"];?>_bulk.xlsx</p>
            <a href="../admin.php" class="a">back to dashboard <i class="fas fa-long-arrow-alt-right"></i></a>   
    <?php
        }
        else {   
    ?>
        <p class="a mini-title popped">Export Success! View the excel file at folder: htdocs / controller / Excel / exported_db.xlsx</p>
        <a href="../admin.php" class="a">back to dashboard <i class="fas fa-long-arrow-alt-right"></i></a>   
    </div>


</body>
</html>
<?php
        }
    }
    else {
        header('location: ./login.php');
    }
?>
