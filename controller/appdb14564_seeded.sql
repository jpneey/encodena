-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2019 at 09:29 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appdb14564`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` int(11) NOT NULL,
  `date_in` timestamp NOT NULL DEFAULT current_timestamp(),
  `date` varchar(255) NOT NULL,
  `applicant_name` varchar(255) NOT NULL,
  `applicant_address` varchar(255) NOT NULL,
  `applicant_email` varchar(255) NOT NULL,
  `applicant_phone_number` varchar(255) NOT NULL,
  `applicant_birthday` varchar(255) NOT NULL,
  `applicant_position_applied` varchar(255) NOT NULL,
  `applicant_referrer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registry`
--

CREATE TABLE `registry` (
  `person_id` int(11) NOT NULL,
  `date_in` timestamp NOT NULL DEFAULT current_timestamp(),
  `date` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sss` varchar(255) NOT NULL,
  `philhealth` varchar(255) NOT NULL,
  `pagibig` varchar(255) NOT NULL,
  `tin` varchar(255) NOT NULL,
  `nbi` varchar(255) NOT NULL,
  `police_clearance` varchar(255) NOT NULL,
  `civil_status` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registry`
--

INSERT INTO `registry` (`person_id`, `date_in`, `date`, `fname`, `mname`, `lname`, `name`, `birthday`, `address`, `mobile`, `email`, `sss`, `philhealth`, `pagibig`, `tin`, `nbi`, `police_clearance`, `civil_status`, `gender`, `position`) VALUES
(1, '2019-10-03 03:40:11', '2019-10-03', '', '', '', 'Rico Doe', '1999-06-24', 'Cainta Rizal', '09999999999', 'sample@email.me', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'male', 'Website Dev'),
(2, '2019-10-03 03:44:46', '2019-10-03', '', '', '', 'Jane Doe', '2019-10-26', 'Pasig', '923112312', 'jane@doe.com', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'female', 'Model'),
(8, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(9, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(10, '2019-10-03 03:40:11', '2019-10-03', '', '', '', 'Rico Doe', '1999-06-24', 'Cainta Rizal', '09999999999', 'sample@email.me', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'male', 'Website Dev'),
(11, '2019-10-03 03:44:46', '2019-10-03', '', '', '', 'Jane Doe', '2019-10-26', 'Pasig', '923112312', 'jane@doe.com', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'female', 'Model'),
(12, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(13, '2019-10-03 03:40:11', '2019-10-03', '', '', '', 'Rico Doe', '1999-06-24', 'Cainta Rizal', '09999999999', 'sample@email.me', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'male', 'Website Dev'),
(14, '2019-10-03 03:44:46', '2019-10-03', '', '', '', 'Jane Doe', '2019-10-26', 'Pasig', '923112312', 'jane@doe.com', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'female', 'Model'),
(15, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(16, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(17, '2019-10-03 03:40:11', '2019-10-03', '', '', '', 'Rico Doe', '1999-06-24', 'Cainta Rizal', '09999999999', 'sample@email.me', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'male', 'Website Dev'),
(18, '2019-10-03 03:44:46', '2019-10-03', '', '', '', 'Jane Doe', '2019-10-26', 'Pasig', '923112312', 'jane@doe.com', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'female', 'Model'),
(19, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(20, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(21, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(22, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(23, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(24, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(25, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(26, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(27, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(28, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(29, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(30, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(31, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(32, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(33, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(34, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(35, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(36, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(37, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(38, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(39, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(40, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(41, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(42, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(43, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(44, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(45, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(46, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(47, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(48, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(49, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(50, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(51, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(52, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(53, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(54, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(55, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(56, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(57, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(58, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(59, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(60, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(61, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(62, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(63, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(64, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(65, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(66, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(67, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(68, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(69, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(70, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(71, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(72, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(73, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(74, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(75, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(76, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(77, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(78, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(79, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(80, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(81, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(82, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(83, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(84, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(85, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(86, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(87, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(88, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(89, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(90, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(91, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(92, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(93, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(94, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(95, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(96, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(97, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(98, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(99, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(100, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(101, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(102, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(103, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(104, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(105, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(106, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(107, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(108, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(109, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(110, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(111, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(112, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(113, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(114, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(115, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(116, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(117, '2019-10-05 06:26:51', '2019-10-05', 'Joana Pauline', 'Lademora', 'Burato', 'Joana Pauline Lademora Burato', '2019-10-04', 'Penny lane st, Valley Gold', '09296209056', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'single', 'male', 'Website Developer'),
(118, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(119, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(120, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(121, '2019-10-05 04:44:53', '2019-10-05', 'John Paul', 'Last', 'Middle', 'John Paul Last Middle', '2019-10-26', 'Penny lane st, Valley Gold', '54562134234', 'j@m.e', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'divorced', 'female', 'Website Developer'),
(122, '2019-10-05 06:55:42', '2019-10-05', 'Dummy', 'Middle', 'Last', 'Dummy Middle Last', '2019-10-03', 'Penny lane st, Valley Gold', '52132131222213', 'lala@mail.me', '1111111111', '1111111111', '11111111111', '11111111111', '11111111111', '11111111111', 'married', 'male', 'Website Developer');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `role`) VALUES
(1, 'superadmin', 'superadmin', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registry`
--
ALTER TABLE `registry`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `registry`
--
ALTER TABLE `registry`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
