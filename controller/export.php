<?php 
session_start();
if (!isset($_SESSION["auth"])) { 
    
    header("location: ../index.php");
    exit;
}
require '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once("dbcontroller.php");
$db_handle = new DBController();
    if(isset($_GET["id"])) {
            $id= $_GET["id"];
            $export = $db_handle->runQuery("SELECT * FROM registry WHERE person_id = $id");
            if (!empty($export)) { 
                foreach($export as $key=>$value){
                    $no = $export[$key]["person_id"];
                    $date = $export[$key]["date"];
                    $fname = $export[$key]["fname"];
                    $mname = $export[$key]["mname"];
                    $lname = $export[$key]["lname"];
                    $birthday = $export[$key]["birthday"];
                    $address = $export[$key]["address"];
                    $mobile = $export[$key]["mobile"];
                    $email = $export[$key]["email"];
                    $sss = $export[$key]["sss"];
                    $philheath = $export[$key]["philhealth"];
                    $pagibig = $export[$key]["pagibig"];
                    $tin = $export[$key]["tin"];
                    $nbi = $export[$key]["nbi"];
                    $pclearance = $export[$key]["police_clearance"];
                    $civil_status = $export[$key]["civil_status"];
                    $gender = $export[$key]["gender"];
                }
            }
    }
    else {
        $id = NULL;
        header("location: ../index.php");
        exit;
    }

/* $spreadsheet = new Spreadsheet(); */
$inputFileName = 'Excel/exported_db.xlsx';

/** Load $inputFileName to a Spreadsheet Object  **/
$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);

$sheet = $spreadsheet->getActiveSheet();

$rows = $sheet->getHighestDataRow();
$cell = $sheet->getCell('A'.$rows);
$cellvalue = $cell->getValue();
echo $cellvalue;
if (!empty($cellvalue)) {
    echo '<br>NOT EMPTY';
    $row = $rows+1;
}
else {
    $row = $rows;
    while(empty($cellvalue)) {
        $row--;
        $cells = $sheet->getCell('A'.$row);
        $cellvalue = $cells->getValue();
    }
    $row = $row+1;
    echo '<br>EMPTY';
    echo '<br>'.$row;
    
}

echo '<br>'.$row;


$sheet->insertNewRowBefore($row);

$sheet->setCellValue('A'.$row, $no);
$sheet->setCellValue('B'.$row, $date);
$sheet->setCellValue('C'.$row, $fname);
$sheet->setCellValue('D'.$row, $mname);
$sheet->setCellValue('E'.$row, $lname);
$sheet->setCellValue('F'.$row, $birthday);
$sheet->setCellValue('G'.$row, $address);
$sheet->setCellValue('H'.$row, $mobile);
$sheet->setCellValue('I'.$row, $email);
$sheet->setCellValue('J'.$row, $sss);
$sheet->setCellValue('K'.$row, $philheath);
$sheet->setCellValue('L'.$row, $pagibig);
$sheet->setCellValue('M'.$row, $tin);
$sheet->setCellValue('N'.$row, $nbi);
$sheet->setCellValue('O'.$row, $pclearance);
$sheet->setCellValue('P'.$row, $civil_status);
$sheet->setCellValue('Q'.$row, $gender);

/* $sheet = $spreadsheet->getActiveSheet();
$row = $sheet->getHighestRow()+1;
$sheet->setCellValue('A'.$row, 'Updated'); */


$writer = new Xlsx($spreadsheet);
$writer->save('../controller/excel/exported_db.xlsx');


header("location: ../component/exported.php");

?>