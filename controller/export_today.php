<?php 
session_start();
if (!isset($_SESSION["auth"])) { 
    
    header("location: ../index.php");
    exit;
}
require '../vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require_once("dbcontroller.php");
$db_handle = new DBController();
    if(isset($_GET["today"])) {
                
            $dt = new DateTime();
            $today = $dt->format('Y-m-d');

            $export = $db_handle->runQuery("SELECT * FROM registry WHERE date = '$today'");
            
            if (!empty($export)) { 
                foreach($export as $key=>$value){
                    $no = $export[$key]["person_id"];
                    $date = $export[$key]["date"];
                    $name = $export[$key]["name"];
                    $birthday = $export[$key]["birthday"];
                    $address = $export[$key]["address"];
                    $mobile = $export[$key]["mobile"];
                    $email = $export[$key]["email"];
                    $sss = $export[$key]["sss"];
                    $philheath = $export[$key]["philhealth"];
                    $pagibig = $export[$key]["pagibig"];
                    $tin = $export[$key]["tin"];
                    $nbi = $export[$key]["nbi"];
                    $pclearance = $export[$key]["police_clearance"];
                    $civil_status = $export[$key]["civil_status"];
                    $gender = $export[$key]["gender"];

                    /* $spreadsheet = new Spreadsheet(); */
                    $inputFileName = 'Excel/base/db_today.xlsx';

                    /** Load $inputFileName to a Spreadsheet Object  **/
                    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);

                    $sheet = $spreadsheet->getActiveSheet();
                    $row = $sheet->getHighestRow()+1;
                    $sheet->insertNewRowBefore($row);

                    $sheet->setCellValue('A'.$row, $no);
                    $sheet->setCellValue('B'.$row, $date);
                    $sheet->setCellValue('C'.$row, $name);
                    $sheet->setCellValue('D'.$row, $name);
                    $sheet->setCellValue('E'.$row, $name);
                    $sheet->setCellValue('F'.$row, $birthday);
                    $sheet->setCellValue('G'.$row, $address);
                    $sheet->setCellValue('H'.$row, $mobile);
                    $sheet->setCellValue('I'.$row, $email);
                    $sheet->setCellValue('J'.$row, $sss);
                    $sheet->setCellValue('K'.$row, $philheath);
                    $sheet->setCellValue('L'.$row, $pagibig);
                    $sheet->setCellValue('M'.$row, $tin);
                    $sheet->setCellValue('N'.$row, $nbi);
                    $sheet->setCellValue('O'.$row, $pclearance);
                    $sheet->setCellValue('P'.$row, $civil_status);
                    $sheet->setCellValue('Q'.$row, $gender);

                    $writer = new Xlsx($spreadsheet);
                    $writer->save('../controller/excel/base/db_today.xlsx');

                    echo 'done<br>';
                }
            }
    }
    else {
        $id = NULL;
        header("location: ../index.php");
        exit;
    }


    $inputFileName = 'Excel/base/db_today.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);$writer = new Xlsx($spreadsheet);
    $writer->save('../controller/excel/exported_db_'.$today.'_bulk.xlsx');
    echo 'renamed' . $inputFileName . '<br>';
    $inputFileName = 'Excel/base/db_bulk.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);$writer = new Xlsx($spreadsheet);
    $writer->save('../controller/excel/base/db_today.xlsx');
    echo 'restored' . $inputFileName . '<br>';

header('location: ../component/exported.php?today='.$today);

?>