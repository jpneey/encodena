-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2019 at 09:03 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appdb14564`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE `applicants` (
  `id` int(11) NOT NULL,
  `date_in` timestamp NOT NULL DEFAULT current_timestamp(),
  `date` varchar(255) NOT NULL,
  `applicant_name` varchar(255) NOT NULL,
  `applicant_address` varchar(255) NOT NULL,
  `applicant_email` varchar(255) NOT NULL,
  `applicant_phone_number` varchar(255) NOT NULL,
  `applicant_birthday` varchar(255) NOT NULL,
  `applicant_position_applied` varchar(255) NOT NULL,
  `applicant_referrer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registry`
--

CREATE TABLE `registry` (
  `person_id` int(11) NOT NULL,
  `date_in` timestamp NOT NULL DEFAULT current_timestamp(),
  `date` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sss` varchar(255) NOT NULL,
  `philhealth` varchar(255) NOT NULL,
  `pagibig` varchar(255) NOT NULL,
  `tin` varchar(255) NOT NULL,
  `nbi` varchar(255) NOT NULL,
  `police_clearance` varchar(255) NOT NULL,
  `civil_status` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `industry_1` varchar(255) NOT NULL,
  `industry_2` varchar(255) NOT NULL,
  `industry_3` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `medical` varchar(255) NOT NULL,
  `employment_contract` varchar(255) NOT NULL,
  `orientation` varchar(255) NOT NULL,
  `employee_id` varchar(255) NOT NULL,
  `uniform` varchar(255) NOT NULL,
  `health_permit` varchar(255) NOT NULL,
  `bu` varchar(255) NOT NULL,
  `client` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `current_position` varchar(255) NOT NULL,
  `date_hired` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `employment_status` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `account_manager` varchar(255) NOT NULL,
  `date_requested` varchar(255) NOT NULL,
  `recruiter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registry`
--

INSERT INTO `registry` (`person_id`, `date_in`, `date`, `name`, `birthday`, `address`, `mobile`, `email`, `sss`, `philhealth`, `pagibig`, `tin`, `nbi`, `police_clearance`, `civil_status`, `gender`, `position`, `industry_1`, `industry_2`, `industry_3`, `status`, `medical`, `employment_contract`, `orientation`, `employee_id`, `uniform`, `health_permit`, `bu`, `client`, `branch`, `current_position`, `date_hired`, `region`, `employment_status`, `remarks`, `account_manager`, `date_requested`, `recruiter`) VALUES
(1, '2019-10-03 03:40:11', '2019-10-03', 'Rico Doe', '1999-06-24', 'Cainta Rizal', '09999999999', 'sample@email.me', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'male', 'Website Dev', 'retail/hotel', 'warehouse/log', 'industrial', 'unemployed', 'Healthy', 'unavailable', '1', '0', '1', '0', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default'),
(2, '2019-10-03 03:44:46', '2019-10-03', 'Jane Doe', '2019-10-26', 'Pasig', '923112312', 'jane@doe.com', '55555555', '44444444', '33333333', '22222222', '11111111', '00000000', 'single', 'female', 'Model', 'retail/hotel', 'warehouse/log', 'industrial', 'hired', 'Healthy', 'unavailable', '1', '1', '1', '1', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `role`) VALUES
(1, 'superadmin', 'superadmin', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registry`
--
ALTER TABLE `registry`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `registry`
--
ALTER TABLE `registry`
  MODIFY `person_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
