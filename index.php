<?php 
    session_start();
    require_once("controller/dbcontroller.php");
    $db_handle = new DBController();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <?php 
    include 'component/head.php';
    ?>
</head>
<body>
    <?php 
    include 'component/header.php';
    ?>
    <div class="navbar-spacer"></div>
    <div class="content-wrapper">
        <div class="container zindex0 lato">
            <form id="ajax-form" class="form margin-left color" action="controller/post_form_beta.php" method="POST" enctype="multipart/form-data">
                <p class="title">Applicant Form</p>
                <div>First Name:</div>
                <input required type="text" name="fname" placeholder="first name"/>
                <div>Middle Name:</div>
                <input required type="text" name="mname" placeholder="middle name"/>
                <div>Last Name:</div>
                <input required type="text" name="lname" placeholder="last name"/>
                <div>Birthday:</div>
                <input class="uppercase" required type="date" name="birthday"/>
                <div>Address:</div>
                <input required type="text" name="address" placeholder="applicant address"/>
                <div>Phone number:</div>
                <input required type="number" name="phone_number" placeholder="0000-000-0000"/>
                <div>E-mail:</div>
                <input class="text-none" required type="email" name="email" placeholder="yourmail@mail.com"/>
                
                <div>Gender:</div>
                <select required name="gender">
                    <option value="male" selected>male</option>
                    <option value="female">female</option>
                </select>
                
                <div>Civil Status:</div>
                <select required name="civil_status">
                    <option value="single" selected>single</option>
                    <option value="married">married</option>
                    <option value="divorced">divorced</option>
                    <option value="others">others</option>
                </select>

                <hr>
                
                <div>SSS:</div>
                <input required type="text" name="sss" placeholder="sss number"/>
                <div>Philhealth:</div>
                <input required type="text" name="philhealth" placeholder="Philhealth number"/>
                <div>Pagibig:</div>
                <input required type="text" name="pagibig" placeholder="Pagibig number"/>
                <div>TIN:</div>
                <input required type="text" name="tin" placeholder="TIN number"/>
                <div>NBI:</div>
                <input required type="text" name="nbi" placeholder="NBI number"/>
                <div>Police Clearance:</div>
                <input required type="text" name="police_clearance" placeholder="Police clearance code"/>

                <hr>
                

                <div>Position:</div>
                <input required type="text" name="position" placeholder="preferred job position"/>
                

                <input id="submit" type="submit" value="post"/>
            </form>
        </div>
    </div>

</body>
</html>
<?php ?>
