<?php 
    session_start();
    require_once("controller/dbcontroller.php");
    $db_handle = new DBController();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Encode Na!</title>
	<meta name="author" content="John Paul Burato, John Dominique Engson" />
	<meta name="description" content="" />
	<meta name="keywords"  content=""/>
	<meta name="Resource-type" content="Document" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="" type="image/x-icon">
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link rel="stylesheet" type="text/css" href="styles/main.css" />

	<script src="vendor/jquery.min.js"></script>
    
    <script src="services/ajax.js"></script>
    <script src="services/main.js"></script>

    <?php 
    include 'component/head.php';
    ?>
</head>
<body>
    <section class="log-in">
        <div class="container margintop">
            <form id="" class="form" action="controller/post_login.php" method="POST" enctype="multipart/form-data">
                <p class="title">Log In</p>
                <input required type="text" name="name" placeholder="username"/>
                <input required type="text" name="password" placeholder="password"/>
                <input id="submit" type="submit" value="log in"/>    
                <?php 
                    if(isset($_GET["toast"])) { ?>
                    <p class="auth-error popped">authentication error</p>
                <?php
                        }
                ?>
                <a href="index.php" class="a">back to site <i class="fas fa-long-arrow-alt-right"></i></a>
            </form>
        </div>
    </section>

</body>
</html>
<?php ?>
