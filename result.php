<?php 
    session_start();
    if (isset($_SESSION["auth"])) {
        
        
    require_once("controller/dbcontroller.php");
    $db_handle = new DBController();
    $dt = new DateTime();
    
    if(isset($_GET["action"])) {
        switch($_GET["action"]) {
            case "search":    
                $title = "Search Results";
                $search = $_POST["search"];
                $result = $db_handle->runQuery("select * from registry where name = '$search'");
                
                break;
            case "list":    
                $title = "All application entry";
                $search = $_POST["search"];
                $result = $db_handle->runQuery("SELECT * FROM registry");
                break;
            case "filter":    
                $year = $_POST["year"];
                $month = $_POST["month"];
                $title = $month.'-'.$year." Entries";
                $result = $db_handle->runQuery("SELECT * FROM registry WHERE Year(date_in) = '$year' AND Month(date_in) = '$month'");
                break;
        }
    } 
    else {
        header('location: result.php?action=list');
    }

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    
    <?php 
    include 'component/head.php';
    ?>
    
</head>
<body>
    <?php 
    include 'component/header.php';
    ?>
    <div class="navbar-spacer"></div>
    <div class="content-wrapper">
        <div class="container">
            <?php 
                include 'component/adminpanel.php';
            ?>
            <div class="navbar-spacer"></div>
            <div class="table">
                <p class="title"><?php echo $title ?></p>
                <?php
                if (!empty($result)) { 
                    foreach($result as $key=>$value){
                ?>
                    <div class="rows">
                        <p><?php echo $result[$key]["name"]; ?>, <?php echo $result[$key]["email"]; ?></p>
                        <div class="child-float-right">
                        
                            <a href="controller/export.php?id=<?php echo $result[$key]["person_id"]; ?>"><i class="fas fa-file-export i-green"></i></a>
                            <a href="component/update.php?id=<?php echo $result[$key]["person_id"]; ?>"><i class="fas fa-user-edit i-blue"></i></a>
                            <a href="javascript:void(0);" id="<?php echo $result[$key]["person_id"]; ?>" onclick="alert(this)" href=""><i class="fas fa-trash-alt i-red"></i></a>
                        </div>
                    </div>
                <?php
                    }
                }
                else { ?>
                    <div class="rows">
                        <div class="child-float-right">
                            <p>empty</p>
                        </div>
                    </div>
                <?php
                }
                ?>        
            </div>
        </div>
    </div>
    <script>
        function alert(e) {
            let id = $(e).attr('id');
            if (window.confirm('Delete Entry ?')){ window.location.href = ("controller/delete_form.php?id="+id); }
            else { return false; }


            };
    </script>
</body>
</html>
<?php
}
else {
    header('location: login.php');
}

?>
